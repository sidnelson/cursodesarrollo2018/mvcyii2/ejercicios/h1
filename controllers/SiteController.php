<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Entradas;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionListar()
    {
        $s=Entradas::find()->asArray()->all();
        
        return $this->render("listarTodos", [
            "datos" => $s,
        ]);
    }
    
     public function actionListar1()
    {
        $s=Entradas::find()->all();
        
        return $this->render("listarTodos", [
            "datos" => $s,
        ]);
    }
    
    public function actionListar2()
    {
        $s=Entradas::find()->select(['texto'])->asArray()->all();
        
        return $this->render("listarTodos", [
            "datos" => $s,
        ]);
    }
    
    public function actionListar3()
    {
        $s=Entradas::find()->select(['texto'])->all();
        
        return $this->render("listarTodos", [
            "datos" => $s,
        ]);
    }
    
    public function actionListar4()
    {
        $s=new Entradas();
        
        return $this->render("listarTodos", [
            "datos" => $s->find()->all(),
        ]);
    }
    
    public function actionListar5()
    {
        $s=new Entradas();
        
        return $this->render("listarTodos", [
            "datos" => $s->findOne(1),
        ]);
    }
    
    public function actionListar6()
    {
        return $this->render("listarTodos", [
            "datos" => Yii::$app->db->createCommand("Select * from entradas")->queryAll(),
        ]);
    }
    
    public function actionMostrar()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(),
        ]);
        
        return $this->render('mostrar', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMostraruno()
    {
        return $this->render('mostrarUno', [
            'model' => Entradas::findOne(1),
        ]);
    }
    
}
